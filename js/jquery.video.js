;(function($, window, document, undefined) {
    
    $.fn.videoPlayer = function(options) {
		var VP = $.fn.videoPlayer;
        
        options = $.extend({
            videoContainerWidth: 1000,
            preloadPoster: 'assets/poster.jpg',
            videoSource: 'video/nature.mp4',
            videMarkers: [],
            videMarkersContainerClass: 'markers-list'
        }, options);
        
        var container = this;
        
        console.log(container);
        
        //Append Video to the container
        container.append('<video controls width="' + options.videoContainerWidth + '" preload poster=' + options.preloadPoster + '></video>');
        
        //Append video source
        var selector = '#' + $(container).attr('id') + ' video';
        console.log(selector);
        $(selector).append('<source src="' + options.videoSource + '" type="video/mp4">');
        
        //Add markers buttons
        $('body').append('<ul class="' + options.videMarkersContainerClass + '"></ul>');
        $.each(options.videMarkers, function(i, val) {
            $('.' + options.videMarkersContainerClass).append('<li data-timer=' + val + '><a href="#">' + val + '</a></li>');            
        });
        
        $('.' + options.videMarkersContainerClass + ' li a').on('click', function(e) {
            e.preventDefault();
            alert($(this).attr('data-timer'));
        });
        
        VP.testPublicFunction = function() {
            alert('Test Public Function!');
        }
    }
})(jQuery, window, document);